const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser');
const path = require('path');
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: true }));
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
const posts = [
    {title: 'Title 1', body: 'Body 1' },
    {title: 'Title 2', body: 'Body 2' },
    {title: 'Title 3', body: 'Body 3' },
    {title: 'Title 4', body: 'Body 4' },
]
const user = {
    firstName: 'Son',
    lastName: 'Khunpheakdey',
}
app.get('/', (req, res) => {
    res.render(path.join(__dirname, 'views', 'pages','index'), { user,title:"Home Page" });
})
app.get('/articles', (req, res) => {
    res.render(path.join(__dirname, 'views', 'pages','articles'), { articles: posts,title:"Articles" });
})
app.listen(port, () => {
  console.log(`App listening at port ${port}`)
})